drop database mobileHub;

create database mobileHub;

use mobileHub;

create table Admins ( 
	admin_id integer PRIMARY KEY,
	password varchar(256)
);

create table Avatar(
	avatar_id integer PRIMARY KEY auto_increment,
	title varchar(30),
	avatar varchar(256) 
);

create table Users (
	user_id integer PRIMARY KEY auto_increment, 
	avatar_id integer,
	firstName VARCHAR(30) NOT NULL,
	lastName VARCHAR(30) NOT NULL, 
	phone VARCHAR(20) UNIQUE NOT NULL,
	email VARCHAR(100) UNIQUE NOT NULL,
	password VARCHAR(256),
	active boolean DEFAULT false,
	adminActive boolean Default true, 
	activationToken varchar(256),
	createdOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY (avatar_id) REFERENCES Avatar(avatar_id)
);

create table Addresses(
	address_id integer PRIMARY KEY auto_increment,
	user_id integer,
	pincode integer,
	title varchar(20),
	firstName varchar(30),
	lastName varchar(30),
	phone varchar(20) NOT NULL,
	area varchar(30),
	flatNo varchar(30),
	landmark varchar(50),
	taluka varchar(20),
	district varchar(20),
	state varchar(20),
	createdOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY (user_id) REFERENCES Users(user_id)
);

create table ProductImages(
	image_id integer PRIMARY KEY auto_increment,
	image1 varchar(256),
	image2 varchar(256),
	image3 varchar(256)
);

create table Brand(
	brand_id integer PRIMARY KEY auto_increment,
	name varchar(20),
	logo varchar(256),
	email varchar(50),
	password varchar(256),
	active boolean Default 1
);

create table Category(
	category_id integer PRIMARY KEY auto_increment,
	brand_id integer,
	name varchar(30),
	image varchar(256),
	active boolean Default 1,
	FOREIGN KEY (brand_id) REFERENCES Brand(brand_id)
);

create table Product(
	product_id integer PRIMARY KEY auto_increment,
	category_id integer,
	image_id integer,
	name varchar(50),
	description longtext,
	weight decimal,
	display_type varchar(256),
	resolution varchar(256),
	os varchar(256),
	cpu varchar(256),
	gpu varchar(100),
	internal_memory integer,
	ram integer,
	front_camera decimal,
	back_camera decimal,
	battery integer,
	price decimal,
	bluetooth varchar(100),
	nfc boolean,
	usb varchar(100),
	wlan varchar(256),
	quantity integer,
	isFlagship boolean,
	active boolean Default 1,
	FOREIGN KEY (category_id) REFERENCES Category(category_id),
	FOREIGN KEY (image_id) REFERENCES ProductImages(image_id),
);


create table Review(
	review_id integer PRIMARY KEY auto_increment,
	user_id integer,	
	product_id integer,
	rating integer,
	review longtext,
	image varchar(256),
	createdOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY (user_id) REFERENCES Users(user_id),
	FOREIGN KEY (product_id) REFERENCES Product(product_id)
);

create table Cart(
	cart_id integer PRIMARY KEY auto_increment,
	user_id integer,
	product_id integer,
	quantity integer,
	FOREIGN KEY (user_id) REFERENCES Users(user_id),
	FOREIGN KEY (product_id) REFERENCES Product(product_id)
);

create table Wishlist(
	wishlist_id integer PRIMARY KEY auto_increment,
	user_id integer,
	title varchar(50),
	createdOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY (user_id) REFERENCES Users(user_id)
);

create table WishlistProducts(
	wishlistProduct_id integer PRIMARY KEY auto_increment,
	wishlist_id integer,
	product_id integer,
	FOREIGN KEY (wishlist_id) REFERENCES Wishlist(wishlist_id),
	FOREIGN KEY (product_id) REFERENCES Product(product_id)
);

create table UserOrder(
	userOrder_id integer PRIMARY KEY auto_increment,
	user_id integer,
	tax decimal,
	paymentType varchar(30),
	paymentStatus boolean,
	deliveryStatus varchar(20),
	createdOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY (user_id) REFERENCES Users(user_id)
);

create table OrderDetails(
	orderDetail_id integer PRIMARY KEY auto_increment,
	userOrder_id integer,
	product_id integer,
	quantity integer,
	createdOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY (userOrder_id) REFERENCES UserOrder(userOrder_id),
	FOREIGN KEY (product_id) REFERENCES Product(product_id)
); 

create table oldPhone(
	phone_id integer PRIMARY KEY auto_increment,
	user_id integer,
	image_id integer,
	brand varchar(50),
	model varchar(50),
	price decimal,
	description longtext,
	active boolean Default 1,
	FOREIGN KEY (user_id) REFERENCES Users(user_id),
	FOREIGN KEY (image_id) REFERENCES ProductImages(image_id)
);