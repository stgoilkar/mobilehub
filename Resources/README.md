# Admin : Completed
## Profile management
    - signin
##  Seller Management
    - Add Seller
    - Active/Deactivate seller
## User management
    - Active/Deactivate user
    - Add avatar
## Review Management
    - Remove review
## Show Statistic(future scope)


# Seller : Completed
## Profile Management
    - signin
    - update brand logo
    - update password
    - forgot password
    - update profile
## Category Management
    - Add category
    - update category
    - active/deactive category
    - list category
## Product Management
    - list Product
    - add product
    - update product
    - update quantity
    - update price
    - add colors and images
    - Active/Deactive product


# User
## Profile Management :c
    - Signup
    - Signin
    - Activation mail
    - Deactivate/activate Account
    - Forgot Password
    - Reset Password
    - Update profile
    - List Avatar
    - Choose Avatar 
    - Update Avatar(same as choose avatar)
    - Keep me signed in
## Address Management :c
    - list Address
    - Add Address
    - Edit Address
    - remove address
## WishList Management :c
    - Add Product in wishlist
    - add group 
    - list all wishlist group
    - List Product in Wishlist
    - Remove Product
    - Move to bag
    - remove wishlist group
    - edit wishlist group name
## Bag Management :c
    - Add Product
    - remove Product
    - List Cart Items
    - Update Quantity
    - Move to wishlist
## Order Management
    - Place Order
    - Track Order
    - Get all orders
    - payment
    - payment Receipt
## Review Management
    - show review
    - Add Review
    - Delete Review
    - upload Photo
    - view photo
## old phone management :c
    - Add phone
    - remove phone
    - update phone
    - list phone


# Website :Completed
## Website Management
    - View Flagship phone
    - Select brand and model name---all details will be shown
    - Get details based on product_id
    - get all products (phone finder)
    - get all brands
    - view image logo
    - view image product
    - view image category
    - get all categories in that brand
    - view products by category
    - view products by brand
    - view category by brand
    - view product by product_id
    - list old phones 
    - old phone by id