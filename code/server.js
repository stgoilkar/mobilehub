const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const jwt = require('jsonwebtoken')
const config = require('./config')

const userRoute = require('./routes/user')
const adminRoute = require('./routes/admin')
const sellerRoute = require('./routes/seller')
const websiteRoute = require('./routes/website')

const { response } = require('express')

const app = express()
app.use(bodyParser.json())
app.use(morgan('combined'))


app.use((request, response, next) => {
    if (request.url == "/user/signin" || request.url == "/user/signup" || request.url.startsWith("/user/forgot_password") ||
        request.url == "/admin/signin" || request.url == "/admin/signup" || request.url.startsWith('/admin/image/') ||
        request.url.startsWith('/seller/image/') || request.url.startsWith('/user/image/') ||
        request.url == "/seller/signin" || request.url.startsWith("/seller/forgot_password") ||
        request.url.startsWith('/user/activateAccount/') ||
        request.url.startsWith('/website')) {
        next()
    } else {
        try {
            const token = request.headers['token']
            if (request.url.startsWith('/user')) {
                const data = jwt.verify(token, config.secret)
                request.userid = data['user_id']
            } else if (request.url.startsWith('/admin')) {
                const data = jwt.verify(token, config.admin_secret)
                request.adminid = data['admin_id']
            } else {
                const data = jwt.verify(token, config.seller_secret)
                request.sellerid = data['brand_id']
            }


            next()
        } catch (ex) {
            response.status(401)
            response.send('You are not allowed to access')
        }
    }
})

app.use('/user', userRoute)
app.use('/admin', adminRoute)
app.use('/seller', sellerRoute)
app.use('/website', websiteRoute)


app.use(express.static('images/'))

app.get('/', (request, response) => {
    response.send('Welcome to MobileHub')
})

app.listen(8080, '0.0.0.0', () => {
    console.log('Server started on port 8080')
})