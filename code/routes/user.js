const express = require('express')
const db = require('../db')
const utils = require('../utils')
const config = require('../config')
const crypto = require('crypto-js')
const jwt = require('jsonwebtoken')
const uuid = require('uuid')
const multer = require('multer')
const fs = require('fs')
const path = require('path')
const mailer = require('../mailer')
const uploadProduct = multer({ dest: 'images/product/' })
const router = express.Router()


//--------------------------------------------------------------------------------------------------------------------------------------------
//                                                               Profile Management
//--------------------------------------------------------------------------------------------------------------------------------------------


router.post('/signup', (request, response) => {
    const { firstName, lastName, email, phone, password } = request.body
    const encryptedPassword = crypto.SHA256(password)
    const activationToken = uuid.v4()
    const activationLink = `http://localhost:8080/user/activateAccount/${activationToken}`
    const filePath = path.join(__dirname, '/../templates/1/index.html')
    let body = ' ' + fs.readFileSync(filePath)
    body = body.replace('{firstName}', firstName)
    body = body.replace('{lastName}', lastName)
    body = body.replace('{activationLink}', activationLink)
    const statement = `insert into Users (firstName,lastName,email,phone,password,activationToken) values ('${firstName}','${lastName}','${email}','${phone}','${encryptedPassword}','${activationToken}') `
    db.query(statement, (error, data) => {
        mailer.sendEmail(email, 'Welcome to MobileHub', body, (error, info) => {
            console.log(error)
            console.log(info);
            response.send(utils.createResult(error, info))
        })
    })
})


router.get('/activateAccount/:activationToken', (request, response) => {
    const { activationToken } = request.params
    const checkActivationtoken = `select firstName from Users where activationToken='${activationToken}'`
    const statement = `update Users set activationToken='', active=1 where activationToken='${activationToken}'`
    db.query(checkActivationtoken, (error, data) => {
        if (error) {
            response.send(utils.createError(error))
        } else {
            db.query(statement, (error, data) => {
                response.send(utils.createSuccess('your account has been activated'))
            })
        }
    })
})

router.post('/signin', (request, response) => {
    const { email, phone, password } = request.body
    const encryptedPassword = crypto.SHA256(password)

    const statement = `select user_id,firstName,lastName,email,phone from Users where (email='${email}' or phone='${phone}') and password='${encryptedPassword}'`
    db.query(statement, (error, users) => {
        if (error) {
            response.send({ status: 'error', error: error })
        } else {
            if (users.length == 0) {
                response.send({ status: 'error', error: 'User does not exist' })
            } else {
                const user = users[0]
                const token = jwt.sign({ user_id: user['user_id'] }, config.secret)
                response.send({
                    status: 'success',
                    success: {
                        firstName: user['firstName'],
                        lastName: user['lastName'],
                        email: user['email'],
                        phone: user['phone'],
                        token: token
                    }
                })
            }
        }
    })
})


router.put("/activeDeactivateaccount", (request, response) => {
    const { active } = request.body
    const statement = `update Users set active=${active} where user_id=${request.userid}`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
});


router.get("/forgot_password/:email", (request, response) => {
    const { email } = request.params;
    const statement = `select firstName from Users where email = '${email}' and active=1`;

    db.query(statement, (error, users) => {
        if (error) {
            response.send(utils.createError(error));
        } else if (users.length == 0) {
            response.send(utils.createError("User Does Not Exist"));
        } else {
            const user = users[0];
            const otp = utils.generateOTP();
            const body = `Your otp is ${otp}`;

            mailer.sendEmail(email, "Reset your Password", body, (error, info) => {
                response.send(
                    utils.createResult(error, {
                        otp: otp,
                        email: email,
                    })
                );
            });
        }
    });
});

router.put("/resetPassword", (request, response) => {
    const { oldpass, password } = request.body;
    const statementUser = `select firstName from Users where user_id=${request.userid} and password='${crypto.SHA256(oldpass)}'`;

    db.query(statementUser, (error, users) => {
        if (error) {
            response.send(utils.createError(error));
        } else if (users.length == 0) {
            response.send(utils.createError("Old Password Wrong"));
        } else {
            const statement = `update Users set password = '${crypto.SHA256(password)}' where user_id=${request.userid}`;
            db.query(statement, (error, data) => {
                response.send(utils.createResult(error, data));
            });
        }
    });
})

router.put('/updateProfile', (request, response) => {
    const { firstName, lastName, email, phone } = request.body
    const statement = `update Users set firstName='${firstName}', lastName='${lastName}', email='${email}', phone='${phone}' where user_id = ${request.userid}`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.get('/listAvatar', (request, response) => {
    const statement = `select * from Avatar`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.put('/chooseAvatar/:avatar_id', (request, response) => {
    const { avatar_id } = request.params
    const statement = `update Users set avatar_id=${avatar_id} where user_id = ${request.userid}`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.get('/image/user/:filename', (request, response) => {
    const { filename } = request.params
    const file = fs.readFileSync(__dirname + '/../images/avatar/' + filename)
    response.send(file)
})

//--------------------------------------------------------------------------------------------------------------------------------------------
//                                                               Address Management
//--------------------------------------------------------------------------------------------------------------------------------------------


router.get('/getallAddress', (request, response) => {
    const statement = `select address_id,pincode, title, firstName, lastName, phone, area, flatNo, landmark, taluka, district, state  from Addresses where user_id=${request.userid}`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.post('/addAddress', (request, response) => {
    const { pincode, title, firstName, lastName, phone, area, flatNo, landmark, taluka, district, state } = request.body
    const statement = `insert into Addresses (user_id,pincode,title,firstName,lastName,phone,area,flatNo,landmark,taluka,district,state) values (${request.userid},${pincode},'${title}','${firstName}','${lastName}','${phone}','${area}','${flatNo}','${landmark}','${taluka}','${district}','${state}')`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.put("/editAddress/:address_id", (request, response) => {
    const { address_id } = request.params
    const { pincode, title, firstName, lastName, phone, area, flatNo, landmark, taluka, district, state } = request.body
    const statement = `update Addresses set  pincode=${pincode}, title='${title}', firstName='${firstName}', lastName='${lastName}', phone='${phone}', area='${area}', flatNo='${flatNo}', landmark='${landmark}', taluka='${taluka}', district='${district}', state='${state}'  where address_id=${address_id}`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
});

router.delete('/deleteaddress/:address_id', (request, response) => {
    const { address_id } = request.params
    const statement = `delete from Addresses where address_id=${address_id}`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//--------------------------------------------------------------------------------------------------------------------------------------------
//                                                               Wishlist Management
//--------------------------------------------------------------------------------------------------------------------------------------------


router.post('/addtoWishlistGroup', (request, response) => {
    const { title } = request.body

    const statement = `insert into Wishlist (user_id,title) values (${request.userid},'${title}')`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.post('/addtoWishlistitem/:wishlist_id/:product_id', (request, response) => {
    const { wishlist_id, product_id } = request.params

    const statement = `insert into WishlistProducts (wishlist_id,product_id) values (${wishlist_id},${product_id})`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.get('/listAllGroupinWishlist', (request, response) => {
    const statement = `select * from Wishlist where user_id=${request.userid}`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.get('/listAllItems/:wishlist_id', (request, response) => {
    const { wishlist_id } = request.params

    const statement = `select p.product_id,p.name,p.price,i.image1  from Product p inner join WishlistProducts wp 
    on p.product_id=wp.product_id inner join ProductImages i on p.image_id=i.image_id 
    where wp.wishlist_id=${wishlist_id};`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.delete('/deleteWishlistitem/:wishlistProduct_id', (request, response) => {
    const { wishlistProduct_id } = request.params
    const statement = `delete from WishlistProducts where wishlistProduct_id=${wishlistProduct_id}`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.delete('/deleteWishlistGroup/:wishlist_id', (request, response) => {
    const { wishlist_id } = request.params
    const statement = `delete from Wishlist where wishlist_id=${wishlist_id}`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.post('/moveToBag/:product_id', (request, response) => {
    const { product_id } = request.params

    const statement = `insert into Cart (user_id,product_id) values (${request.userid},${product_id})`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.put('/updateGroupName/:wishlist_id', (request, response) => {
    const { wishlist_id } = request.params
    const { title } = request.body
    const statement = `update Wishlist set title='${title}' where wishlist_id=${wishlist_id}`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})



//--------------------------------------------------------------------------------------------------------------------------------------------
//                                                               Bag Management
//--------------------------------------------------------------------------------------------------------------------------------------------


router.post('/addProduct/:product_id', (request, response) => {
    const { product_id } = request.params

    const statement = `insert into Cart (user_id,product_id) values (${request.userid},${product_id})`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.get('/listBagItem', (request, response) => {
    const statement = `select p.product_id,p.name,p.price,i.image1,c.quantity  from Product p inner join Cart c 
    on p.product_id=c.product_id inner join ProductImages i on p.image_id=i.image_id 
    where c.user_id=${request.userid};`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.delete('/removefromBag/:cart_id', (request, response) => {
    const { cart_id } = request.params
    const statement = `delete from Cart where cart_id=${cart_id}`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.put('/updateQuantity/:cart_id', (request, response) => {
    const { cart_id } = request.params
    const { quantity } = request.body
    const statement = `update Cart set quantity=quantity+${quantity} where cart_id=${cart_id}`
    console.log(statement);
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.post('/moveToWishlist/:product_id/:wishlist_id', (request, response) => {
    const { product_id, wishlist_id } = request.params

    const statement = `insert into WishlistProducts (wishlist_id,product_id) values (${wishlist_id},${product_id})`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//--------------------------------------------------------------------------------------------------------------------------------------------
//                                                               Old Phone Management
//--------------------------------------------------------------------------------------------------------------------------------------------

router.post('/addPhone/:image_id', (request, response) => {
    const { image_id } = request.params
    const { brand, model, price, description } = request.body

    const statement = `insert into oldPhone (user_id, brand,model,price,description,image_id) values (${request.userid},'${brand}','${model}',${price},'${description}',${image_id})`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.post('/addImages', uploadProduct.array('image', 3), (request, response) => {
    const images = request.files
    const imageStatement = `insert into ProductImages (image1, image2, image3) values ('${images[0].filename}', '${images[1].filename}','${images[2].filename}')`
    db.query(imageStatement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.delete('/deletePhone/:phone_id', (request, response) => {
    const { phone_id } = request.params
    const statement = `delete from oldPhone where phone_id=${phone_id}`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})


router.put('/updatePhone', (request, response) => {
    const { phone_id } = request.params
    const { brand, model, price, description } = request.body

    const statement = `update oldPhone set user_id=${request.userid}, brand='${brand}',model='${model}',price=${price},description='${description}' where phone_id=${phone_id}`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.put('/updateImages/:image_id', uploadProduct.array('image', 3), (request, response) => {
    const { image_id } = request.params
    const images = request.files

    const selectStatement = `select * from ProductImages where image_id=${image_id}`
    db.query(selectStatement, (error, data) => {
        try {
            fs.unlink(`images/product/${data[0]['image1']}`, (error) => {
                if (error) {
                    throw error
                }
            });

            fs.unlink(`images/product/${data[0]['image2']}`, (error) => {
                if (error) {
                    throw error
                }
            });

            fs.unlink(`images/product/${data[0]['image3']}`, (error) => {
                if (error) {
                    throw error
                }
            });
            console.log("File is deleted.");
        } catch (error) {
            console.log(error);
        }

    })

    const imageStatement = `update ProductImages set image1='${images[0].filename}', image2='${images[1].filename}',image3='${images[2].filename}' where image_id=${image_id}`
    db.query(imageStatement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.get('/listoldPhone', (request, response) => {
    const statement = `select * from oldPhone where user_id=${request.userid}`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})


module.exports = router