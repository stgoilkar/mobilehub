const express = require('express')
const db = require('../db')
const utils = require('../utils')
const config = require('../config')
const crypto = require('crypto-js')
const jwt = require('jsonwebtoken')
const mailer = require('../mailer')
const fs = require('fs')
const multer = require('multer')
const uploadLogo = multer({ dest: 'images/logo/' })
const uploadCategory = multer({ dest: 'images/category/' })
const uploadProduct = multer({ dest: 'images/product/' })
const { request, response } = require('express')
const router = express.Router()

//--------------------------------------------------------------------------------------------------------------------------------------------
//                                                               Profile Management
//--------------------------------------------------------------------------------------------------------------------------------------------

router.post('/signin', (request, response) => {
    const { email, password } = request.body
    const encryptedPassword = crypto.SHA256(password)

    const statement = `select brand_id,name,logo,email from Brand where email='${email}' and password='${encryptedPassword}' and active=1`
    db.query(statement, (error, sellers) => {
        if (error) {
            response.send({ status: 'error', error: error })
        } else {
            if (sellers.length == 0) {
                response.send({ status: 'error', error: 'Enter correct Credentials' })
            } else {
                const seller = sellers[0]
                const token = jwt.sign({ brand_id: seller['brand_id'] }, config.seller_secret)
                response.send({
                    status: 'success',
                    success: {
                        name: seller['name'],
                        logo: seller['logo'],
                        email: seller['email'],
                        token: token
                    }
                })
            }
        }
    })
})

router.put('/uploadlogo/:brand_id', uploadLogo.single('image'), (request, response) => {
    const { brand_id } = request.params
    const fileName = request.file.filename

    const statement = `update Brand set logo = '${fileName}' where brand_id = ${brand_id}`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.get('/image/logo/:filename', (request, response) => {
    const { filename } = request.params
    const file = fs.readFileSync(__dirname + '/../images/logo/' + filename)
    response.send(file)
})


router.get("/forgot_password/:email", (request, response) => {
    const { email } = request.params;
    const statement = `select name from Brand where email = '${email}'`;
    console.log(statement);
    db.query(statement, (error, sellers) => {
        if (error) {
            response.send(utils.createError(error));
        } else if (sellers.length == 0) {
            response.send(utils.createError("seller Does Not Exist"));
        } else {
            const seller = sellers[0];
            const otp = utils.generateOTP();
            const body = `Your otp is ${otp}`;

            mailer.sendEmail(email, "Reset your Password", body, (error, info) => {
                response.send(
                    utils.createResult(error, {
                        otp: otp,
                        email: email,
                    })
                );
            });
        }
    });
});

router.put("/updatePassword", (request, response) => {
    const { oldpass, password } = request.body;
    const statementSeller = `select name from Brand where brand_id=${request.sellerid} and password='${crypto.SHA256(oldpass)}'`;

    db.query(statementSeller, (error, sellers) => {
        if (error) {
            response.send(utils.createError(error));
        } else if (sellers.length == 0) {
            response.send(utils.createError("Old Password Wrong"));
        } else {
            const statement = `update Brand set password = '${crypto.SHA256(password)}' where brand_id=${request.sellerid}`;
            db.query(statement, (error, data) => {
                response.send(utils.createResult(error, data));
            });
        }
    });
})

router.put('/updateProfile', (request, response) => {
    const { name, email } = request.body
    const statement = `update Brand set name = '${name}',email='${email}' where brand_id = ${request.sellerid}`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//--------------------------------------------------------------------------------------------------------------------------------------------
//                                                               category management
//--------------------------------------------------------------------------------------------------------------------------------------------

router.get('/getallCategory', (request, response) => {
    const statement = `select category_id,name,image,active from Category where brand_id=${request.sellerid}`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.post('/addCategory', uploadCategory.single('image'), (request, response) => {
    const { name } = request.body
    const fileName = request.file.filename

    const statement = `insert into Category  (image,name,brand_id) values ('${fileName}', '${name}',${request.sellerid}) `
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.put('/updateCategory/:category_id', uploadCategory.single('image'), (request, response) => {
    const { category_id } = request.params
    const { name } = request.body
    const fileName = request.file.filename

    const statement = `update Category set image='${fileName}',name='${name}' where category_id=${category_id} `
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.put("/activeDeactivateCategory/:category_id", (request, response) => {
    const { category_id } = request.params
    const { active } = request.body
    const statement = `update Category set active=${active} where category_id=${category_id}`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
});

router.get('/image/category/:filename', (request, response) => {
    const { filename } = request.params
    const file = fs.readFileSync(__dirname + '/../images/category/' + filename)
    response.send(file)
})


//--------------------------------------------------------------------------------------------------------------------------------------------
//                                                               Product Management
//--------------------------------------------------------------------------------------------------------------------------------------------

router.get('/getallProductbyCategory/:category_id', (request, response) => {
    const { category_id } = request.params
    const statement = `select product_id, name, description, price,isFlagship, active, quantity from Product where category_id=${category_id}`
    db.query(statement, (error, product) => {
        response.send(utils.createResult(error, product))
    })
})

router.post('/addProduct/:imageId', (request, response) => {
    const { imageId } = request.params
    const { category_id, name, description, weight, display_type, resolution, os, cpu, gpu, internal_memory, ram, front_camera, back_camera, battery, price, bluetooth, nfc, usb, wlan, quantity } = request.body
    const statement = `insert into Product (image_id,category_id,name, description, weight, display_type, resolution, os, cpu, gpu, internal_memory, ram, front_camera, back_camera, battery, price, bluetooth, nfc, usb, wlan, quantity) values (${imageId},${category_id},'${name}', '${description}', ${weight}, '${display_type}', '${resolution}', '${os}', '${cpu}', '${gpu}', ${internal_memory}, ${ram}, ${front_camera}, ${back_camera}, ${battery}, ${price}, '${bluetooth}', ${nfc}, '${usb}', '${wlan}', ${quantity}) `
    console.log(statement);
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.post('/addImages', uploadProduct.array('image', 3), (request, response) => {
    const images = request.files
    const imageStatement = `insert into ProductImages (image1, image2, image3) values ('${images[0].filename}', '${images[1].filename}','${images[2].filename}')`
    db.query(imageStatement, (error, data) => {
        response.send(utils.createResult(error,data))
    })
})

router.get('/image/product/:filename', (request, response) => {
    const { filename } = request.params
    const file = fs.readFileSync(__dirname + '/../images/product/' + filename)
    response.send(file)
})

router.put('/updateQuantity/:productId', (request, response) => {
    const { productId } = request.params
    const { quantity } = request.body
    const statement = `update Product set quantity=quantity+${quantity} where product_id=${productId}`
    console.log(statement);
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.put('/updatePrice/:productId', (request, response) => {
    const { productId } = request.params
    const { price } = request.body
    const statement = `update Product set price=${price} where product_id=${productId}`
    console.log(statement);
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.put("/activeDeactivateProduct/:product_id", (request, response) => {
    const { product_id } = request.params
    const { active } = request.body
    const statement = `update Product set active=${active} where product_id=${product_id}`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
});


router.put('/updateProduct/:product_id', (request, response) => {
    const { product_id } =request.params
    const { category_id,name, description, weight, display_type, resolution, os, cpu, gpu, internal_memory, ram, front_camera, back_camera, battery, bluetooth, nfc, usb, wlan } = request.body
    const statement = `update Product set category_id=${category_id},name='${name}', description='${description}', weight=${weight}, display_type='${display_type}', resolution='${resolution}', os='${os}', cpu='${cpu}', gpu='${gpu}', internal_memory=${internal_memory}, ram=${ram}, front_camera=${front_camera}, back_camera=${back_camera}, battery=${battery}, bluetooth='${bluetooth}', nfc=${nfc}, usb='${usb}', wlan='${wlan}' where product_id=${product_id}`
    console.log(statement);
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.put("/updateFlagship/:product_id", (request, response) => {
    const { product_id } = request.params
    const removeFlagship= `update Product set isFlagship=0 where category_id IN (select category_id from Category where brand_id=${request.sellerid})`
    
    db.query(removeFlagship, (error, data) => {
        if(error){
            response.send(utils.createError(error))
        } else{
            const statement = `update Product set isFlagship=1 where product_id=${product_id}`
            db.query(statement,(error,data) => {
                response.send(utils.createResult(error,data))
            })
        }
    })
});


module.exports = router