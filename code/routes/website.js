const express = require('express')
const db = require('../db')
const utils = require('../utils')
const config = require('../config')
const crypto = require('crypto-js')
const jwt = require('jsonwebtoken')
const mailer = require('../mailer')
const multer = require('multer')
const fs = require('fs')
const uploadLogo = multer({ dest: 'images/logo/' })
const uploadCategory = multer({ dest: 'images/category/' })
const uploadProduct = multer({ dest: 'images/product/' })
const { request, response } = require('express')
const router = express.Router()

router.get('/ListFlagships', (request, response) => {
    const statement = `Select p.name as model_name, p.description,b.name as brand_name,i.image1 as image 
  from Product p inner join Category c on p.category_id = c.category_id
  inner join Brand b on c.brand_id = b.brand_id
  inner join Color co on p.product_id = co.product_id
  inner join ProductImages i on i.image_id=co.image_id where p.isFlagship=1`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.get('/listAllBrands', (request, response) => {
    const statement = `Select brand_id, name, logo from Brand`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.get('/image/logo/:filename', (request, response) => {
    const { filename } = request.params
    const file = fs.readFileSync(__dirname + '/../images/logo/' + filename)
    response.send(file)
})

router.get('/image/product/:filename', (request, response) => {
    const { filename } = request.params
    const file = fs.readFileSync(__dirname + '/../images/product/' + filename)
    response.send(file)
})

router.get('/image/category/:filename', (request, response) => {
    const { filename } = request.params
    const file = fs.readFileSync(__dirname + '/../images/category/' + filename)
    response.send(file)
})

router.get('/listAllProductbyBrand/:brand_id', (request, response) => {
    const { brand_id } = request.params
    const statement = `Select product_id, name from Product where category_id IN (select category_id from Category where brand_id=${brand_id})`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.get('/product/:product_id', (request, response) => {
    const { product_id } = request.params
    const productStatement = `Select p.name, p.description, p.weight, p.display_type, p.resolution, p.os, p.cpu, p.gpu, p.internal_memory, p.ram, p.front_camera, p.back_camera, p.battery, p.bluetooth, p.nfc, p.usb, p.wlan, i.image1, i.image2, i.image3 from Product p inner join ProductImages i on p.image_id=i.image_id where p.product_id=${product_id}`
    db.query(productStatement, (error, product) => {
        response.send(utils.createResult(error, product))
    })
})

router.get('/listAllProducts', (request, response) => {
    const statement = `Select p.name, p.description, p.weight, p.display_type, p.resolution, p.os, p.cpu, p.gpu, p.internal_memory, p.ram, p.front_camera, p.back_camera, p.battery, p.bluetooth, p.nfc, p.usb, p.wlan, i.image1, i.image2, i.image3 from Product p inner join ProductImages i on p.image_id=i.image_id`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.get('/listCategoryByBrand/:brandId', (request, response) => {
    const { brandId } = request.params
    const statement = `Select category_id, name from Category where brand_id=${brandId}`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.get('/listProductByCategory/:categoryId', (request, response) => {
    const { categoryId } = request.params
    const statement = `Select p.name, p.description, p.weight, p.display_type, p.resolution, p.os, p.cpu, p.gpu, p.internal_memory, p.ram, p.front_camera, p.back_camera, p.battery, p.bluetooth, p.nfc, p.usb, p.wlan, i.image1, i.image2, i.image3 from Product p inner join ProductImages i on p.image_id=i.image_id where category_id=${categoryId}`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.get('/listOldPhones', (request, response) => {
    const statement = `Select o.brand, o.model, o.price,o.description,i.image1
    from oldPhone o inner join ProductImages i on i.image_id=o.image_id`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.get('/OldPhone/:phone_id', (request, response) => {
    const { phone_id } = request.params
    const statement = `Select o.brand, o.model, o.price, o.description, u.firstName, u.lastName, u.email, u.phone, i.image1, i.image2, i.image3
    from oldPhone o inner join Users u on o.user_id=u.user_id
    inner join ProductImages i on i.image_id=o.image_id where o.phone_id=${phone_id}`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})


module.exports = router