const express = require('express')
const db = require('../db')
const utils = require('../utils')
const config = require('../config')
const crypto = require('crypto-js')
const jwt = require('jsonwebtoken')
const mailer = require('../mailer')
const multer = require('multer')
const fs = require('fs')
const upload = multer({ dest: 'images/avatar/' })
const { request, response } = require('express')
const router = express.Router()

//--------------------------------------------------------------------------------------------------------------------------------------------
//                                                               Profile Management
//--------------------------------------------------------------------------------------------------------------------------------------------

router.post('/signup', (request, response) => {
    const { admin_id, password } = request.body
    const encryptedPassword = crypto.SHA256(password)
    const statement = `insert into Admins (admin_id, password) values (${admin_id},'${encryptedPassword}') `
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})


router.post('/signin', (request, response) => {
    const { admin_id, password } = request.body
    const encryptedPassword = crypto.SHA256(password)

    const statement = `select admin_id from Admins where admin_id='${admin_id}' and password='${encryptedPassword}'`
    db.query(statement, (error, admins) => {
        if (error) {
            response.send({ status: 'error', error: error })
        } else {
            if (admins.length == 0) {
                response.send({ status: 'error', error: 'Enter correct Credentials' })
            } else {
                const admin = admins[0]
                const token = jwt.sign({ admin_id: admin['admin_id'] }, config.admin_secret)
                response.send({
                    status: 'success',
                    success: {
                        token: token
                    }
                })
            }
        }
    })
})


//--------------------------------------------------------------------------------------------------------------------------------------------
//                                                               seller management
//--------------------------------------------------------------------------------------------------------------------------------------------

router.post('/addseller', (request, response) => {
    const { name, email, password } = request.body
    const encryptedPassword = crypto.SHA256(password)

    let body = `Email=${email}<br> Password=${password}`

    const statement = `insert into Brand (name, email, password) values ('${name}','${email}','${encryptedPassword}') `
    db.query(statement, (error, data) => {
        if (error) {
            response.send(utils.createError(error))
        }
        mailer.sendEmail(email, `Welcome ${name} to MobileHub`, body, (error, info) => {
            console.log(error)
            console.log(info)
            response.send(utils.createResult(error, data))
        })
    })
})

router.put("/activeDeactivateSeller/:brand_id", (request, response) => {
    const { brand_id } = request.params
    const { active } = request.body
    const statement = `update Brand set active=${active} where brand_id=${brand_id}`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
});


//--------------------------------------------------------------------------------------------------------------------------------------------
//                                                               User Management
//--------------------------------------------------------------------------------------------------------------------------------------------

router.put("/activeDeactivateuser/:user_id", (request, response) => {
    const { user_id } = request.params
    const { active } = request.body
    const statement = `update Users set adminActive=${active} where user_id=${user_id}`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
});

router.post('/addAvatar', upload.single('image'), (request, response) => {
    const { title } = request.body
    const fileName = request.file.filename

    const statement = `insert into Avatar  (avatar,title) values ('${fileName}', '${title}');`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.get('/getallAvatar', (request, response) => {
    const statement = `select * from Avatar;`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.get('/image/avatar/:filename', (request, response) => {
    const { filename } = request.params
    const file = fs.readFileSync(__dirname + '/../images/avatar/' + filename)
    response.send(file)
})


//--------------------------------------------------------------------------------------------------------------------------------------------
//                                                               Review Management
//--------------------------------------------------------------------------------------------------------------------------------------------

router.delete('/deleteReview/:review_id', (request, response) => {
    const { review_id } = request.params
    const statement = `delete from Review where review_id=${review_id}`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
});

//--------------------------------------------------------------------------------------------------------------------------------------------
//                                                               Order Management
//--------------------------------------------------------------------------------------------------------------------------------------------

router.put('/updatePaymentStatus/:userOder', (request, response) => {
    const { status } = request.body
    const { userOder_id } = request.params
    const statement = `update UserOrder set paymentStatus=${status} where userOder_id=${userOder_id}`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
});

router.put('/updateDeliveryStatus/:userOder', (request, response) => {
    const { status } = request.body
    const { userOder_id } = request.params
    const statement = `update UserOrder set deliveryStatus=${status} where userOder_id=${userOder_id}`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
});

router.get('/listUserOrders', (request, response) => {
    const statement = `Select * from UserOrder`
    db.query(statement,(error,data) => {
        response.send(error,data)
    })
})

module.exports = router