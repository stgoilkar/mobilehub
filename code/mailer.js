const mailer = require('nodemailer')
const config = require('./config')

function sendEmail(email, subject, body, callback) {
  const transport = mailer.createTransport({
    service: 'gmail',
    auth: {
      user: config.email,
      pass: config.password
    }
  })

  const options = {
    from: config.email,
    to: email,
    subject: subject,
    html: body
  }

  transport.sendMail(options, callback)
}

module.exports = {
  sendEmail: sendEmail
}